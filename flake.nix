{
  description = "NixOS Flake";

  inputs = {
    /**
     * Default Nixpkgs
     * 
     * Set the default nixpkgs version
     * Use packages with pkgs.mypackage
     */
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    
    /**
     * Other Nixpkgs
     * 
     * Specific nixpkgs versions independent from the default
     * Use packages with pkgs.stable.mypackage or pkgs.unstable.mypackage
     */
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    /**
     * Nix User Repositories
     * 
     * Set of unofficial package repositories
     * Use packages with pkgs.nur.repos.{user}.{repo}.{package}
     * e.g.: `pkgs.nur.repos.rycee.firefox-addons.ublock-origin`
     */
    nur.url = "github:nix-community/nur";
    nur.inputs.nixpkgs.follows = "nixpkgs";

    /**
     * Hardware Modules
     * 
     * Provides hardware specific default configuration.
     * See host configuration (hosts/default.nix) for usage
     */
    nixos-hardware.url = "github:nixos/nixos-hardware/master";

    /**
     * Lanzaboote
     * 
     * Enable Secure Boot for NixOS
     */
    lanzaboote.url = "github:nix-community/lanzaboote/v0.4.2";
    lanzaboote.inputs.nixpkgs.follows = "nixpkgs";

    /**
     * Home Manager
     * 
     * Use nix to manage dotfiles and user specific packages
     * See user configuration (users/default.nix) for usage
     */
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs:
  let
    lib = inputs.nixpkgs.lib;
    gen = import ./lib/generators.nix { inherit lib; inherit inputs; };

    /**
     * Import users and convert to the following format:
     * user = { name = "user"; config = { ... }}
     * 
     * Now users can be referenced inside hosts
     */
    users = import ./config/users |> lib.mapAttrs lib.nameValuePair;
    hardwareModules = inputs.nixos-hardware.nixosModules;
  in {
    nixosConfigurations = import ./config/hosts {
      inherit users;
      inherit hardwareModules;
    } |> lib.mapAttrs gen.mkHost;
  };
}
