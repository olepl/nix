{ lib, pkgs, ... }:
{
  imports = [];

  modules = {
    fetch.enable = true;
    git.enable = true;
    starship.enable = true;
    nushell.enable = true;
    kitty.enable = true;
    ghostty.enable = true;
    vscode.enable = true;
    zed.enable = true;
    brave.enable = true;
    "1password".enable = true;
    cursor.enable = true;
    cursor.style = "Banana";
    firefox.enable = true;
    obsidian.enable = true;
    spotify.enable = true;
  };

  home.packages = with pkgs; [
    # minecraft
  ];

  home.file = {
    # "filepath".source = local/path/sourcefile.txt;
    # "filepath".text = ''multiline text'';
  };

  home.sessionVariables = {
    # VARIABLE = "value";
  };

  home.sessionPath = [
    # "$HOME/.local/bin"
  ];
}
