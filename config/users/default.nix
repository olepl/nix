/**
 * Define users:
 *
 * username = {
 *   fullName = "Full Name";
 *   config = ./username/home.nix
 * }
 */
{
  olep = {
    fullName = "Ole Plechinger";
    config = ./olep/home.nix;
  };
}
