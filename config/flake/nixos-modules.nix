inputs: [
  /**
   * Import nixos modules from inputs
   * e.g.: home-manager.nixosModules.default
   */
  inputs.nur.modules.nixos.default
  inputs.home-manager.nixosModules.default
  inputs.lanzaboote.nixosModules.lanzaboote
]
