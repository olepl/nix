{ inputs }: [
  /**
   * Import nixpkgs overlays from inputs
   * e.g.: inputs.nur.overlays.default
   */
  inputs.nur.overlays.default

  /**
   * Override attributes for specific packages
   */
  (final: prev: {
    # example = prev.example.overrideAttrs (oldAttrs: rec {
    # ...
    # });
  })

  /**
   * Add specific versions of nixpkgs
   */
  (final: prev: {
    unstable = import inputs.nixpkgs-unstable {
      system = final.system;
      config = final.config;
    };
    stable = import inputs.nixpkgs-stable {
      system = final.system;
      config = final.config;
    };
  })
]
