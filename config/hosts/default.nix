/**
 * Define hosts:
 *
 * hostname = {
 *   hardware = hardwaremodule;
 *   config = ./hostname/host.nix
 *   users = [
 *     user1
 *     user2
 *   ]
 * }
 */
{ users, hardwareModules }: with users; with hardwareModules;
{
  fw13 = {
    hardware = framework-11th-gen-intel;
    config = ./fw13/host.nix;
    users = [
      olep
    ];
  };
}
