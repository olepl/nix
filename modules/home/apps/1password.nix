{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules."1password";
in {
  options.modules."1password" = {
    enable = mkEnableOption "1password";
  };

  config = mkIf cfg.enable {
    programs.ssh = {
      enable = true;
      extraConfig = ''
        Host *
            IdentityAgent ~/.1password/agent.sock
      '';
    };
    systemd.user.services = {
      "1password" = {
        Unit = {
          Description = "1Password Linux desktop client";
          Documentation ="https://support.1password.com/";
          After = "graphical-session.target";
        };
        Install = {
          WantedBy = [ "graphical-session.target" ];
        };
        Service = {
          ExecStart = "${pkgs._1password-gui}/bin/1password --silent";
        };
      };
    };
  };
}
