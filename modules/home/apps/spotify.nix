{ lib, pkgs, config, ... }: with lib;
let
cfg = config.modules.spotify;
in {
  options.modules.spotify = {
    enable = mkEnableOption "spotify daemon";
  };

  config = mkIf cfg.enable {
    services.spotifyd.enable = true;
  };
}
