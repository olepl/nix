{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.cursor;
in {
  options.modules.cursor = {
    enable = mkEnableOption "cursor";

    style = mkOption {
      type = types.strMatching "^(none|Bibata|Banana|macOS)$";
      default = "Bibata";
    };
  };

  config = mkIf cfg.enable {
    gtk.enable = true;
    
    home.pointerCursor = mkIf (cfg.style != "none") (mkMerge [
      (mkIf (cfg.style == "Bibata") {
        name = "Bibata-Modern-Classic";
        package = pkgs.bibata-cursors;
        size = 24;
        gtk.enable = true;
      })
      (mkIf (cfg.style == "Banana") {
        name = "Banana";
        package = pkgs.banana-cursor;
        size = 28;
        gtk.enable = true;
      })
      (mkIf (cfg.style == "macOS") {
        name = "macOS";
        package = pkgs.apple-cursor;
        size = 28;
        gtk.enable = true;
      })
    ]);
  };
}
