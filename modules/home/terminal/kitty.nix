{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.kitty;
in {
  options.modules.kitty = {
    enable = mkEnableOption "kitty";
  };

  config = mkIf cfg.enable {
    programs.kitty = {
      enable = true;
      settings = {
        font_family = "Cascadia Code NF";
        font_features = "Cascadia Code NF +calt +ss01 +ss20";
        font_size = 11;

        # symbol_map = lib.concatStrings [
        #   "U+E5FA-U+E6B1,"                              # Seti-UI + Custom
        #   "U+E700-U+E7C5,"                              # Devicons
        #   "U+F000-U+F2E0,"                              # Font Awesome
        #   "U+E200-U+E2A9,"                              # Font Awesome Extension
        #   "U+F0001-U+F1AF0,"                            # Material Design Icons
        #   "U+E300-U+E3E3,"                              # Weather
        #   "U+F400-U+F532,U+2665,U+26A1,"                # Octicons
        #   "U+E0A0-U+E0A2,U+E0B0-U+E0B3,"                # Powerline Symbols
        #   "U+E0A3,U+E0B4-U+E0C8,U+E0CA,U+E0CC-U+E0D4,"  # Powerline Extra Symbols
        #   "U+23FB-U+23FE,U+2b58,"                       # IEC Power Symbols
        #   "U+F300-U+F372,"                              # Font Logos
        #   "U+E000-U+E00A,"                              # Pomicons
        #   "U+EA60-U+EBEB "                              # Codicons
        #   "CaskaydiaCove Nerd Font"
        # ];

        undercurl_style = "thick-dense";

        cursor = "#73b7f2";
        cursor_shape = "beam";

        url_style = "straight";

        window_padding_width = 15;
        placement_strategy = "top-left";

        hide_window_decorations = "yes";

        # Colors
        foreground = "#ffffff";
        background = "#000000";

        selection_foreground = "none";
        selection_background = "#163356";

        # black
        color0 = "#484f58";
        color8 = "#6e7681";

        # red
        color1 = "#ff7b72";
        color9 = "#ffa198";

        # green
        color2  = "#3fb950";
        color10 = "#56d364";

        # yellow
        color3  = "#d29922";
        color11 = "#e3b341";

        # blue
        color4  = "#58a6ff";
        color12 = "#79c0ff";

        # magenta
        color5  = "#bc8cff";
        color13 = "#d2a8ff";

        # cyan
        color6  = "#39c5cf";
        color14 = "#56d4dd";

        # white
        color7  = "#b1bac4";
        color15 = "#f0f6fc";

        wayland_titlebar_color = "background";
      };
    };
  };
}
