{ lib, pkgs, config, ... }: with lib;
let
cfg = config.modules.ghostty;
in {
  options.modules.ghostty = {
    enable = mkEnableOption "ghostty";
  };

  config = mkIf cfg.enable {
    programs.ghostty.enable = true;
    programs.ghostty.settings = {
      font-family = [ "Cascadia Code NF" ];
      font-feature = [ "calt" "ss01" "ss20" ];
      font-size = 11;
      
      clipboard-read = "allow";
      clipboard-paste-protection = false;

      window-theme = "ghostty";
      window-decoration = "none";
      window-padding-x = 8;
      window-padding-y = 8;

      palette = [
         "0=#484f58"
         "1=#ff7b72"
         "2=#3fb950"
         "3=#d29922"
         "4=#58a6ff"
         "5=#bc8cff"
         "6=#39c5cf"
         "7=#b1bac4"
         "8=#6e7681"
         "9=#ffa198"
        "10=#56d364"
        "11=#e3b341"
        "12=#79c0ff"
        "13=#d2a8ff"
        "14=#56d4dd"
        "15=#f0f6fc"
      ];
      foreground = "#ffffff";
      background = "#000000";
      selection-foreground = "#ffffff";
      selection-background = "#163356";
      
      cursor-color = "#73b7f2";
      adjust-cursor-thickness = 3;
    };
  };
}
