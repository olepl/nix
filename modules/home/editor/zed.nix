{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.zed;
in {
  options.modules.zed = {
    enable = mkEnableOption "zed editor";
  };

  config = mkIf cfg.enable {
    programs.zed-editor = {
      enable = true;

      userSettings = {
        theme = "Ayu Dark";
        telemetry = {
          diagnostics = false;
          metrics = false;
        };
        ui_font_size = 16;
        buffer_font_size = 16;
        autosave = "on_focus_change";
        auto_update = false;
        buffer_font_features = {
          "calt" = true;
          "ss01" = true;
          "ss20" = true;
        };
        buffer_font_fallbacks = [ "CaskaydiaCove Nerd Font" ];
      };

      userKeymaps = [
        # {
        #   context = "Workspace";
        #   bindings = {
        #     ctrl-shift-t = "workspace::NewTerminal";
        #   };
        # };
      ];

      extensions = [];

    };
  };
}
