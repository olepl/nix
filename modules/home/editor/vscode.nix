{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.vscode;
in {
  options.modules.vscode = {
    enable = mkEnableOption "vscode";
  };

  config = mkIf cfg.enable {
    programs.vscode.enable = true;
    programs.vscode.package = pkgs.vscodium;
    programs.vscode.mutableExtensionsDir = true;

    programs.vscode.profiles.default = {
      enableUpdateCheck = false;
      enableExtensionUpdateCheck = false;

      userSettings = {
        "update.showReleaseNotes" = false;
        "explorer.confirmDelete" = false;
        "explorer.confirmDragAndDrop" = false;
        "workbench.colorTheme" = "GitHub Dark Default";
        "files.autoSave" = "onFocusChange";
        "editor.fontFamily" = "'Cascadia Code NF', 'Cascadia Code'";
        "editor.stickyTabStops" = true;
        "editor.fontLigatures" = "'calt', 'ss01', 'ss20'";
        "errorLens.fontStyleItalic" = true;
        "editor.minimap.autohide" = true;
        "editor.lineNumbers" = "relative";
        "files.insertFinalNewline" = true;
        "files.trimFinalNewlines" = true;
        "workbench.startupEditor" = "none";
        "workbench.activityBar.location" = "bottom";
        "window.confirmSaveUntitledWorkspace" = false;
        "window.titleBarStyle" = "custom";
        "explorer.incrementalNaming" = "smart";
        "terminal.external.linuxExec" = "ghostty";
        "terminal.integrated.enablePersistentSessions" = false;
        "terminal.integrated.fontLigatures" = "'calt', 'ss01', 'ss20'";
        "terminal.integrated.cursorWidth" = 2;
        "terminal.integrated.enableImages" = true;
        "terminal.integrated.shellIntegration.decorationsEnabled" = "overviewRuler";
        "terminal.integrated.stickyScroll.enabled" = true;
        "git.openRepositoryInParentFolders" = "never";
        "git.confirmSync" = false;
        "nix.enableLanguageServer" = true;
        "nix.serverPath" = "nixd";
        "nix.hiddenLanguageServerErrors" = [
          "textDocument/definition"
        ];
        "nix"."serverSettings"."nixd"."diagnostic"."suppress" = [
          "sema-unused-def-lambda-noarg-formal"
          "sema-unused-def-let"
          "sema-extra-with"
        ];
      };

      extensions = with pkgs.vscode-extensions; [
        github.github-vscode-theme
        jnoortheen.nix-ide
        mads-hartmann.bash-ide-vscode
        bmalehorn.vscode-fish
        thenuprojectcontributors.vscode-nushell-lang
        ms-python.python
        ms-toolsai.jupyter-renderers
        ms-toolsai.jupyter-keymap
        ms-toolsai.vscode-jupyter-cell-tags
        ms-toolsai.vscode-jupyter-slideshow
        twxs.cmake
        formulahendry.code-runner
        ms-vscode.hexeditor
        # vadimcn.vscode-lldb
        rust-lang.rust-analyzer
        bungcip.better-toml
        a5huynh.vscode-ron
        serayuzgur.crates
        naumovs.color-highlight
        usernamehw.errorlens
        tomoki1207.pdf
        yzhang.markdown-all-in-one
        bierner.markdown-preview-github-styles
        myriad-dreamin.tinymist
        #>jeanp413.open-remote-ssh
        #>metaphore.kanagawa-vscode-color-theme
      ];

      keybindings = [
        {
          # run default test task, build task is already ctrl+shift+b by default
          key = "ctrl+shift+alt+b";
          command = "workbench.action.tasks.test";
          when = "taskCommandsRegistered";
        }
      ];

      userTasks = {
        # version = "2.0.0";
        # tasks = [
        #   {
        #     type = "shell";
        #     label = "Hello task";
        #     command = "hello";
        #   }
        # ];
      };

      globalSnippets = {
        # fixme = {
        #   prefix = [
        #     "fixme"
        #   ];
        #   body = [
        #     "$LINE_COMMENT FIXME: $0"
        #   ];
        #   description = "Insert a FIXME remark";
        # };
      };

      languageSnippets = {

      };
    };

    home.packages = with pkgs; [
      nixd
      nixfmt-rfc-style
    ];
  };
}
