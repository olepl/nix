{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.starship;
in {
  options.modules.starship = {
    enable = mkEnableOption "starship prompt";
    dualLine = mkEnableOption "dual line prompt";
  };

  config = mkIf cfg.enable {
    programs.starship = {
      enable = true;
      settings = {
        add_newline = false;

        format = mkMerge [
          (mkIf cfg.dualLine (
            lib.concatStrings [
              "$directory"
              "$git_branch"
              "$git_status"
              "$git_state"
              "$git_commit"
              "$git_metrics"
              "$docker_context"
              "$package"
              "$fill"
              "$cmd_duration"
              "$time"
              "$line_break"
              "$battery"
              "($username$hostname )"
              "$jobs"
              "$shell"
              "$character"
            ])
          )
          (mkIf (!cfg.dualLine) (
            lib.concatStrings [
              "$battery"
              "($username$hostname )"
              "$jobs"
              "$directory"
              "$shell"
              "$character"
            ])
          )
        ];

        right_format = mkIf (!cfg.dualLine) (
          lib.concatStrings [
          "$git_branch"
          "$git_status"
          "$git_state"
          "$git_commit"
          "$git_metrics"
          "$docker_context"
          "$package"
          "$cmd_duration"
          "$time"
        ]);

        ## Prompt Line ##

        username.format = "[$user]($style)";

        hostname = {
          format = "@[$ssh_symbol$hostname]($style)";
          style = "bold bright-green";
        };

        #shell.disabled = false

        #character = {
        #  success_symbol = "[»](bold green)"
        #  error_symbol = "[»](bold red)"
        #};

        ## Header Line ##

        directory.truncation_length = 5;

        git_branch.format = "[on](fg:bright-white) [$branch(:$remote_branch)]($style)";

        git_status = {
          format = "([$conflicted$stashed$deleted$renamed$modified$untracked$staged( $ahead_behind)]($style)) ";
          modified = "*";
          conflicted = "!";
          diverged = "⇡$\{ahead_count}⇣$\{behind_count}";
        };

        package = {
          format = "[$symbol$version]($style) ";
          symbol = "";
        };

        ## Right Prompt ##

        fill.symbol = " ";

        cmd_duration.format = "[took](bright-white) [$duration]($style) ";

        time = {
          disabled = true;
          format = "[$time]($style)";
          time_format = "%D %H:%M";
          style = "bold bright-black";
        };
      };
    };
  };
}
