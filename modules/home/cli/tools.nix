{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.cli;
in {
  options.modules.cli = {
    enable = mkEnableOption "modern cli tools";
  };

  config = mkIf cfg.enable {
    programs = {
      zoxide.enable = true;
      carapace.enable = true;
      broot.enable = true;
      eza.enable = true;
      eza.enableNushellIntegration = false;
      micro.enable = true;
      tealdeer.enable = true;
      navi.enable = true;
      navi.settings.client.tealdeer = true;
      thefuck.enable = true;
      bat.enable = true;
      fd.enable = true;
      fzf.enable = true;
      ripgrep.enable = true;
      jq.enable = true;
      bottom.enable = true;
      zellij.enable = true;
    };

    services = {
      pueue.enable = true;
    };

    home.packages = with pkgs; [
      cht-sh
      wiki-tui

      delta

      uutils-coreutils

      timg

      dust
      duf

      sd
      choose

      curlie
      dogdns
      hyperfine
      gping
      procs
      glances
      mprocs
    ];
  };
}
