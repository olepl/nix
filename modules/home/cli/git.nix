{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.git;
in {
  options.modules.git = {
    enable = mkEnableOption "git";
  };

  config = mkIf cfg.enable {
    programs.git = {
      enable = true;
      lfs.enable = true;
      userName = "Ole Plechinger";
      userEmail = "ole.plechinger@protonmail.com";

      extraConfig = {
        init.defaultBranch = "main";
        push.autoSetupRemote = true;
        pull.rebase = false;

        # 1Passwort Signing
        gpg.format = "ssh";
        user.signingkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHDQJXLX94LEru3CNUatFifnFb+TKl45aKOCnLXqlejf";
        "gpg \"ssh\"".program = "${getExe' pkgs._1password-gui "op-ssh-sign"}";
        commit.gpgsign = true;
      };

      delta.enable = true;
    };
  };
}
