{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.nushell;
in {
  options.modules.nushell = {
    enable = mkEnableOption "nushell";
  };

  config = mkIf cfg.enable {
    modules.cli.enable = true;

    programs.nushell = {
      enable = true;
      configFile.text = ''
          $env.config.show_banner = false
              
          $env.config.history = {
              file_format: "sqlite"
          }
          $env.config.cursor_shape = {
              emacs: line
              vi_insert: block
              vi_normal: underscore
          }
          $env.config.use_kitty_protocol = true
          $env.config.completions.algorithm = "fuzzy"
          $env.config.rm.always_trash = false

          $env.config.keybindings ++= [
              {
                  name: history_menu
                  modifier: alt
                  keycode: char_r
                  mode: [emacs vi_normal vi_insert]
                  event: [
                      { send: menu name: history_menu }
                  ]
              }
          ]

          def openconnect-univpn [] {
              echo $"(op item get UniID --fields label=password --reveal)\n(op item get UniID --otp)"
              | sudo openconnect --useragent=AnyConnect --no-external-auth --user=(op item get UniID --fields label=username) --passwd-on-stdin vpn-ac.urz.uni-heidelberg.de
          }

          def update-librechat [] {
              cd ~/Workspaces/LibreChat
              docker compose down
              git pull
              docker compose pull
              docker compose up -d
          }

          def update-open-webui [] {
              docker rm -f open-webui
              docker pull ghcr.io/open-webui/open-webui:main
              systemctl restart docker-open-webui.service
          }
      '';
      envFile.text = ''

      '';

      extraConfig = ''
          export def navi_widget [] {
              let current_input = (commandline)
              let last_command = ($current_input | navi fn widget::last_command | str trim)

              match ($last_command | is-empty) {
                  true => {^navi --print | complete | get "stdout"}
                  false => {
                      let find = $"($last_command)_NAVIEND"
                      let replacement = (^navi --print --query $'($last_command)' | complete | get "stdout")

                      match ($replacement | str trim | is-empty) {
                          false => {$"($current_input)_NAVIEND" | str replace $find $replacement}
                          true => $current_input
                      }
                  }
              } 
              | str trim
              | commandline edit --replace $in
              
              commandline set-cursor --end
          }

          let nav_keybinding = {
              name: "navi",
              modifier: control,
              keycode: char_r,
              mode: [emacs, vi_normal, vi_insert],
              event: {
                  send: executehostcommand,
                  cmd: navi_widget,
              }
          }

          $env.config.keybindings = ($env.config.keybindings | append $nav_keybinding)
      '';

      shellAliases = config.modules.aliases;
      environmentVariables = config.modules.env;
    };
    home.packages = [ pkgs.openconnect ];
  };
}
