{ lib, pkgs, config, ... }: with lib;
{
  home.packages = with pkgs; [
    conda
    bacon
    sccache
    python3
    irust
  ];
}
