{ lib, config, ...}: with lib;
{
  options.modules.aliases = mkOption { type = types.attrs; };

  config.modules.aliases = {
    # Navigation
    z  = "z";
    zi = "zi";
    zt = "br";
    cd = "z";
    ci = "zi";
    ct = "br";

    # Listing Files
    l     = "eza --icons";
    ll    = "eza --icons -l";
    l1    = "eza --icons -1";
    la    = "eza --icons -a";
    lla   = "eza --icons -la";
    l1a   = "eza --icons -1a";
    lt    = "eza --icons -T";
    l1t   = "eza --icons -1T";
    llt   = "eza --icons -lT";
    lat   = "eza --icons -aT";
    llat  = "eza --icons -laT";
    l1at  = "eza --icons -1aT";
    tree  = "eza --icons -T";

    # Git
    g     = "git";
    ga    = "git add";
    gaa   = "git add -A";
    gc    = "git commit -m";
    gi    = "git status";
    "g<"  = "git pull";
    "g>"  = "git push";

    # Coreutils alternatives
    cat   = "bat";
    diff  = "delta";
    fz    = "fzf";
    code  = "codium";

    # Other
    fetch = "fastfetch";
  };
}
