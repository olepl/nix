{ lib, config, ...}: with lib;
{
  options.modules.env = mkOption { type = types.attrs; };

  config.modules.env = {
    FLAKE = "./nixos";
  };
}
