{ lib, pkgs, config, ... }: with lib;
{
  home.packages = with pkgs; [
    tor-browser
  ];
}
