{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.brave;
in {
  options.modules.brave = {
    enable = mkEnableOption "brave";
  };

  config = mkIf cfg.enable {
    programs.chromium = {
      enable = true;
      package = pkgs.brave;

      extensions = [
        { id = "aeblfdkhhhdcdjpifhhbdiojplfjncoa"; } # 1password
        { id = "dphilobhebphkdjbpfohgikllaljmgbn"; } # simple login
        { id = "cdglnehniifkbagbbombnjghhcihifij"; } # kagi search
        { id = "gebbhagfogifgggkldgodflihgfeippi"; } # youtube dislikes
        { id = "mnjggcdmjocbbbhaepdhchncahnbgone"; } # youtube sponsorblock
        { id = "khncfooichmfjbepaaaebmommgaepoid"; } # youtube unhook
        { id = "mmioliijnhnoblpgimnlajmefafdfilb"; } # shazam
      ];
    };
  };
}
