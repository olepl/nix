{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.firefox;
in {
  options.modules.firefox = {
    enable = mkEnableOption "firefox";
  };

  config = mkIf cfg.enable {
    # TODO
    home.packages = [ pkgs.arkenfox-userjs ];
    programs.firefox = {
      enable = true;

      # arkenfox.enable = true;
      # arkenfox.version = "128.0";

      policies = {};

      profiles.default = {
        name = "default";
        id = 0;

        # arkenfox = {
        #   enable = true;
        #   "0000".enable = true;
        #   "0100".enable = true;
        #   "0200".enable = true;
        #   "0300".enable = true;
        #   "0400".enable = false;
        #   "0600".enable = true;
        #   "0700".enable = true;
        #   "0800".enable = true;
        #   "0800"."0830".enable = false;
        #   "0900".enable = true;
        #   "1000".enable = true;
        #   "1200".enable = true;
        #   "1600".enable = true;
        #   "1700".enable = true;
        #   "2000".enable = true;
        #   "2400".enable = true;
        #   "2600".enable = true;
        #   "2700".enable = true;
        #   "2800".enable = true;
        #   "4000".enable = true;
        # };

        settings = {
          "browser.shell.checkDefaultBrowser" = false;
          "extensions.autoDisableScopes" = 0;
          "browser.aboutwelcome.enabled" = false;
          "browser.search.separatePrivateDefault" = true;
          "browser.search.separatePrivateDefault.ui.enabled" = false;


        };
        extraConfig = "";

        search = {
          default = "Kagi";
          privateDefault = "DuckDuckGo";

          engines = {

          };
          order = [];

          force = true;
        };

        bookmarks = [];

        extensions.packages = with pkgs.nur.repos.rycee.firefox-addons; [
          ublock-origin
          kagi-search
          onepassword-password-manager
          simplelogin
          sponsorblock
          return-youtube-dislikes
        ];
        # settings = {
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.addon@simplelogin" = true;
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org" = true;
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.search@kagi.com" = true;
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.sponsorBlocker@ajay.app" = true;
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net" = true;
        #   "extensions.webextensions.ExtensionStorageIDB.migrated.{762f9885-5a13-4abd-9c77-433dcd38b8fd}" = true;
        # };

        userChrome = "";
        userContent = "";
      };
    };
  };
}
