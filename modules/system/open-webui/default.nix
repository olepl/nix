{ lib, pkgs, config, ... }: with lib;
let
cfg = config.modules.open-webui;
in {
  options.modules.open-webui = {
    enable = mkEnableOption "Open WebUI";
  };

  config = mkIf cfg.enable {

    # TODO Auto update containers:
    # update-containers = pkgs.writeShellScriptBin "update-containers" ''
    #   SUDO=""
    #   if [[ $(id -u) -ne 0 ]]; then
    #     SUDO="sudo"
    #   fi

    #     images=$($SUDO ${pkgs.podman}/bin/podman ps -a --format="{{.Image}}" | sort -u)

    #     for image in $images
    #     do
    #       $SUDO ${pkgs.podman}/bin/podman pull $image
    #     done
    # '';

    # systemd.services = {
    #   # ...
    #   updatecontainers = {
    #     serviceConfig = {
    #       Type = "oneshot";
    #       ExecStart = "update-containers";
    #     };
    #   };
    #   # ...
    # };

    virtualisation.oci-containers.backend = "docker";
    virtualisation.oci-containers.containers.open-webui = {
      ports = [ "3000:8080" ];
      volumes = [ "open-webui:/app/backend/data" ];
      image = "ghcr.io/open-webui/open-webui:main";
      extraOptions = [
        "--privileged=true"
        # "--security-opt=seccomp=unconfined"
        # "--mount=type=bind,source=/sys/fs/cgroup,target=/sys/fs/cgroup,readonly=false"
        # "--security-opt=apparmor=unconfined"
      ];

      environment = {
        # General Setup
        WEBUI_AUTH = "False";
        ENABLE_OPENAI_API = "False";
        ENABLE_OLLAMA_API = "False";
        
        # Evaluation
        ENABLE_EVALUATION_ARENA_MODELS = "False";
        ENABLE_MESSAGE_RATING = "False";
        
        # Default Models
        # DEFAULT_MODELS = "";
        # TASK_MODEL_EXTERNAL = "";
        # TASK_MODEL = "";

        # Documents
        # RAG_EMBEDDING_ENGINE = "openai";
        # RAG_EMBEDDING_MODEL = "text-embedding-3-small";
        # PDF_EXTRACT_IMAGES = "True";

        # Web Search
        ENABLE_RAG_WEB_SEARCH = "True";
        RAG_WEB_SEARCH_ENGINE = "tavily";
        RAG_WEB_SEARCH_RESULT_COUNT = "5";
        TAVILY_API_KEY = "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";
        
        # Image Generation
        ENABLE_IMAGE_GENERATION = "False";
        IMAGE_GENERATION_ENGINE = "openai";
        IMAGE_GENERATION_MODEL = "dall-e-3";
        IMAGE_SIZE = "512x512";
      };
    };
  };
}
