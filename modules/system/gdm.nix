{ lib, pkgs, config, ... }: with lib;
let
  cfg = config.modules.gdm;
in {
  options.modules.gdm = {
    enable = mkEnableOption "gdm";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      bibata-cursors
    ];

    programs.dconf.profiles.gdm.databases = [{
      settings."org/gnome/desktop/interface" = {
        "cursor-theme" = "Bibata-Modern-Classic";
      };
    }];

    systemd.tmpfiles.rules = [
      "L+ /run/gdm/.config/monitors.xml - - - - ${pkgs.writeText "gdm-monitors.xml" ''
        <monitors version="2">
          <configuration>
            <layoutmode>logical</layoutmode>
            <logicalmonitor>
              <x>0</x>
              <y>0</y>
              <scale>2</scale>
              <primary>yes</primary>
              <monitor>
                <monitorspec>
                  <connector>eDP-1</connector>
                  <vendor>BOE</vendor>
                  <product>0x095f</product>
                  <serial>0x00000000</serial>
                </monitorspec>
                <mode>
                  <width>2256</width>
                  <height>1504</height>
                  <rate>59.999</rate>
                </mode>
              </monitor>
            </logicalmonitor>
          </configuration>
          <configuration>
            <layoutmode>physical</layoutmode>
            <logicalmonitor>
              <x>0</x>
              <y>0</y>
              <scale>2</scale>
              <primary>yes</primary>
              <monitor>
                <monitorspec>
                  <connector>eDP-1</connector>
                  <vendor>BOE</vendor>
                  <product>0x095f</product>
                  <serial>0x00000000</serial>
                </monitorspec>
                <mode>
                  <width>2256</width>
                  <height>1504</height>
                  <rate>59.999</rate>
                </mode>
              </monitor>
            </logicalmonitor>
          </configuration>
        </monitors>
      ''}"
    ];
  };
}
