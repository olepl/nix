<div align="center">

<img align="center" width="150" height="150" src="./assets/nixos.png" alt="Logo"/>

# A NixOS Flake

</div>

## File Structure
```python
├─📁 assets       # wallpapers, logos, etc.
├─📁 config
│  ├─📁 flake     # configure flake inputs, overlays and modules
│  ├─📁 hosts     # configure devices and hosts
│  └─📁 users     # configure users homes
├─📁 lib          # utility functions
├─📁 modules
│  ├─📁 home      # home manager modules
│  └─📁 system    # nixos modules
├─📄 flake.nix    # entry point of this flake
└─ ...
```

## Installation

## Configuration

## Usage

## Inner workings

### Users and Hosts
