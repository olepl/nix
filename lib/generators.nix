/**
 * Generators for users, sysusers and hosts
 */
{ inputs, lib }: with lib;
rec {
  recImport = import ./auto-import.nix { inherit lib; };

  /** Import all custom system and home modules from modules/system/ and modules/home */
  
  nixosModules = import ../config/flake/nixos-modules.nix inputs
                  ++ recImport { path = ../modules/system; };

  homeModules  = import ../config/flake/home-modules.nix  inputs
                  ++ recImport { path = ../modules/home;   };

  /**
   * Generate users from their name and conig file
   * You can also specify default options for all users here
   *
   * Users are defined in config/users/default.nix
   */
  mkUser = name: config: {
    imports = [ config ] ++ homeModules;

    programs.home-manager.enable = true;
    home.stateVersion = mkDefault "23.11";

    home.username = name;
    home.homeDirectory = "/home/" + name;
  };

  /**
   * Generate system users
   * You can also specify default options for all system users here
   *
   * System Users are defined as part of config/users/default.nix
   */
  mkSystemUser = name: config: {
    isNormalUser = true;
    description = config.fullName;
    extraGroups = [ "networkmanager" "wheel" "docker" ];
  };

  /**
   * Generate hosts from the given users and configurations
   * 
   * Host are defined in config/hosts/default.nix,
   * referencing user definitions from config/users/default.nix
   */
  mkHost = hostname: { hardware, system ? "x86_64-linux", config, users }:
  let
    /**
     * Use the list of users as in:
     * [ { name = "user"; config = { ... }}
     *   { name = "user"; config = { ... }}
     * ]
     * to reconstruct them as attribute sets:
     * { user = { ... }
     *   user = { ... }
     * }
     */
    users' = users |> builtins.listToAttrs;

    /**
     * Default configuration for all hosts
     */
    defaultConfig = {lib, pkgs, ...}: {
      # Enable necessary experimental features and set base version (do not change!)
      nix.settings.experimental-features = "nix-command flakes pipe-operators";
      system.stateVersion = mkDefault "23.11";

      # Set hostname
      networking.hostName = hostname;
      
      # Apply overlays
      nixpkgs = {
        overlays = import ../config/flake/overlays.nix { inherit inputs; };
        config.allowUnfree = true;
      };

      # Enable home manager for all users and generate their configuration with mkUser
      home-manager = {
        useGlobalPkgs = true;
        users = users'
          |> mapAttrs (n: c: c.config)
          |> mapAttrs mkUser;
      };

      # Enable nh
      programs.nh.enable = true;

      # Enable all users to use 1password system authentication
      programs._1password-gui.polkitPolicyOwners =
        users' |> mapAttrsToList (n: c: n); # extract just the names

      # Set default shell and generate system user configuration with mkSystemUser
      users.defaultUserShell = pkgs.nushell;
      users.users = users'
          |> mapAttrs mkSystemUser;

      # Install basic default packages
      environment.systemPackages = with pkgs; [
        nushell
        sbctl
      ];

      # Enable lanzaboote for Secure Boot
      boot.loader.systemd-boot.enable = lib.mkForce false;
      boot.lanzaboote = {
        enable = true;
        pkiBundle = "/var/lib/sbctl";
      };
    };
  in nixosSystem {
    /**
     * Put all the modules together
     */
    inherit system;
    modules = [
      hardware config defaultConfig
    ] ++ nixosModules;
  };
}
